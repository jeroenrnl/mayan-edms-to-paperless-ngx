Mayan EDMS to Paperless-NGX migration script
============================================

This is a simple script that does the following:
- Create tags in Paperless as they are in Mayan
- Create document types in Paperless as they are in Mayan
- Attempt to create Storage Paths from Cabinets
- Download documents from Mayan and import them in Paperless, applying tags and document types.
- Apply storage paths to the documents

It will upload the documents in batches of increasing sizes of 1, 2, 4, 8 and 16, once the batches are 16 it will remain uploading batches of 16. This is to give you the chance to make corrections to the automatically assigned tags. This will improve the hit rate for the later documents.

The Storage Paths feature in Paperless is significantly different from the Cabinets feature in Mayan, so your results may vary. The storage paths are applied all in one go, *after* the document upload has completed. This is due to the fact that it cannot be applied until the document has finished processing and waiting for the processing to finish slowed down the upload significantly. The script will prompt you to to wait for the processing to finish.

## How to use ##
- Create tokens in both Mayan and Paperless, since both are based on Django, the process is identical: add ```/admin``` to the url and create a token for a user that has access to all documents.
- Modify the ```migrate.py``` script, to fill in URL and Token for each application
- Set ```mayan.v4``` to ```False``` for versions older than v4.0
- Run ```./migrate.py``` to start the process.
- The script will migrate all tags, document types and cabinets first. This can take a while.
- Then the script will migrate a single document and pauses. Log in to Paperless and fix any erronously assigned metadata (if you have automatic tagging enabled), correcting it early in the process, will enhance the chance of success later. (although I did have some weird results in the end).
- Once the script has finished migrating all documents, wait for Paperless to finish processing all documents. Then type enter to assign storage paths.
- After storage paths have been assigned, you need to run the ```document_renamer```, see https://docs.paperless-ngx.com/administration/#renamer

Paperless does a good job detecting duplicates and the script will not create any tags or document types that are already there, so it's no problem to kill the script and restart if you need to make corrections.

I've had pretty good results migrating two instances of about 250 documents each, but your kilometrage may vary, no guarantees and ensure you have a good backup before your start.

The storage path assignment does not work on v4.

## Support & Maintenance ##
I have finished migrating to Paperless and have decommissioned my Mayan instances, so I am not able to do any testing, troubleshooting, etc. If you run into an issue, you're on your own. If you do find a problem and fix it, I'll be happy to merge your changes.The script is a one-off (or two-off in my case) that I'm sharing to help anyone that's in the same situation as I am, but I'm not willing to invest more time in it, hope you understand.
