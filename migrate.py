#!/usr/bin/python
import requests
import json
import re

def main():
    mayan.url = "https://mayan.local"
    mayan.token =  " go to /admin to create one "
    paperless.url = "https://paperless.local"
    paperless.token = " go to /admin to create one"

    # Set this to False for older versions
    mayan.v4 = True

    m = mayan()

    print("Getting tags from Mayan...")
    tags = m.getTags()
    print("Getting cabinets from Mayan...")
    cabinets = m.getCabinets()
    print("Getting document types from Mayan...")
    doctypes = m.getDoctypes()
    print("Getting documents from Mayan...")
    docs = m.getDocs()

    p = paperless()
    print("Creating Storage Paths in Paperless...")
    p.createStoragePaths(cabinets)
    print("Creating document types in Paperless...")
    p.createDoctypes(doctypes)
    print("Creating tags in in Paperless...")
    p.createTags(tags)
    counter = 0
    count = 1
    print("Migrating documents to Paperless...")
    for id, doc in docs.items():
        name = doc.get("name")
        print(f'Downloading {name}...', end="")
        counter+=1
        file = m.downloadDoc(id)
        print(f' ...uploading to Paperless')
        p.createDoc(file, doc)
        if counter % count == 0:
            input("Press Enter to continue...")
            if count < 16:
                count *= 2

    input("Wait for all documents to be imported (File Tasks), then press enter")
    for id, doc in docs.items():
        p.setStoragePath(doc)

def patchapi(url, data, headers):
    response = requests.patch(url, data=data, headers=headers)
    return response

def postapi(url, data, headers, files = None):
    response = requests.post(url, data=data, headers=headers, files=files)
    return response

def getapi(url, headers):
    response = requests.get(url, headers=headers)
    return response

def getall(url, auth):
    results = []
    while True:
        response = getapi(url, auth).json()
        results += response.get('results')
        url = response.get('next')
        if not url:
            break
    return results

class mayan:
    url = ""
    token = ""
    tags = {}
    cabinets = {}
    docs = {}
    v4 = False

    def getAPIurls(self):
        if self.v4:
            return {
                "docs": "/api/v4/documents/",
                "tags": "/api/v4/tags/",
                "doctypes": "/api/v4/document_types/",
                "cabinets": "/api/v4/cabinets/",
                "download": "/api/v4/documents/"
            }
        else:
            return {
                "docs": "/api/documents/",
                "tags": "/api/tags/",
                "doctypes": "/api/document_types/",
                "cabinets": "/api/cabinets/",
                "download": "/api/documents/"
            }
    def getDocs(self, force = False):
        if len(self.docs) > 0 and not force:
            return self.docs
        ds = getall(self.url + self.getAPIurls()["docs"], self.getHeaders())
        for d in ds:
            id = d.get('id')
            if self.v4:
                url = d.get("file_latest").get("url") + "download/"
            else:
                url = self.url + self.getAPIurls()["download"] + str(d.get('id')) + "/download/"

            self.docs[id] = {
                'name':             d.get('label'),
                'document_type':    d.get('document_type').get('id'),
                'storage_path':     d.get('storage_path'),
                'url':              url,
                'tags':             [],
                'cabinets':         []
            }
            for tid, tag in self.tags.items():
                if id in tag.get('documents'):
                    self.docs[id]['tags'].append(tid)
            for cid, cabinet in self.cabinets.items():
                if id in cabinet.get('documents'):
                    self.docs[id]['cabinets'].append(cid)
        return self.docs

    def downloadDoc(self, id):
        doc = self.getDocs()[id]
        url = doc.get('url')
        filename = "/tmp/" + doc.get("name")
        file = getapi(url, self.getHeaders())
        open(filename, 'wb').write(file.content)
        return filename

    def getDoctypes(self):
        doctypes = {}
        dt = getall(self.url + self.getAPIurls()["doctypes"], self.getHeaders())
        for d in dt:
            doctypes[d.get('id')] = {
                'name': d.get('label'),
            }
        return doctypes

    def getTags(self):
        if len(self.tags) > 0:
            return self.tags

        ts = getall(self.url + self.getAPIurls()["tags"], self.getHeaders())
        for t in ts:
            docs = []
            documents = getall(t.get('documents_url'), self.getHeaders())
            for d in documents:
                docs.append(d.get('id'))

            self.tags[t.get('id')] = {
                'name':         t.get('label'),
                'colour':       t.get('color'),
                'documents':    docs
            }
        return self.tags

    def getCabinets(self):
        if len(self.cabinets) > 0:
            return self.cabinets

        cs = getall(self.url + self.getAPIurls()["cabinets"], self.getHeaders())
        for c in cs:
            docs = []
            documents = getall(c.get('documents_url'), self.getHeaders())
            for d in documents:
                docs.append(d.get('id'))

            self.cabinets[c.get("id")] = {
                'name':         c.get("label"),
                'full_path':    c.get("full_path"),
                'documents':    docs
            }
        return self.cabinets

    def getHeaders(self):
        return {
            'Authorization': f'Token {self.token}'
        }

class paperless:
    url = ""
    token = ""
    cabinets_sp = {}
    tags_tag = {}
    doctype_doctype = {}
    docs = {}
    storage_paths = {}
    tags = {}
    doctypes = {}

    def getDocs(self, force = False):
        if len(self.docs) > 0 and not force:
            return self.docs
        dt = getall(self.url + "/api/documents/", self.getHeaders())
        for d in dt:
            self.docs[d.get('id')] = {
                'id':               d.get('id'),
                'title':            d.get('title'),
                'tags':             d.get('tags'),
                'document_type':    d.get('document_type'),
                'storage_path':     d.get('storage_path'),
                'url':              self.url + "/api/documents" + str(d.get('id')) + "/download/"
            }
        return self.docs

    def getDoctypes(self, force = False):
        if len(self.doctypes) > 0 and not force:
            return self.doctypes
        dt = getall(self.url + "/api/document_types/", self.getHeaders())
        for d in dt:
            self.doctypes[d.get('id')] = {
                'id':       d.get('id'),
                'name':     d.get('name'),
            }
        return self.doctypes

    def getTags(self, force):
        if len(self.tags) > 0 and not force:
            return self.tags
        ts = getall(self.url + "/api/tags/", self.getHeaders())
        for t in ts:
            self.tags[t.get('id')] = {
                'id':       t.get('id'),
                'name':     t.get('name'),
                'colour':   t.get('color')
            }
        return self.tags

    def getStoragePaths(self, force = False):
        if len(self.storage_paths) > 0 and not force:
            return self.storage_paths
        sps = getall(self.url + "/api/storage_paths/", self.getHeaders())
        for sp in sps:
            self.storage_paths[sp.get('id')] = {
                'id':   sp.get('id'),
                'name': sp.get('name'),
                'path': sp.get('path')
            }
        return self.storage_paths

    def getDocByName(self, name, force = False):
        ds = self.getDocs(force)
        for id, d in ds.items():
            if d.get('title') == name:
                return d

    def getStoragePathByPath(self, path):
        sps = self.getStoragePaths(True)
        for id, sp in sps.items():
            if sp.get('path') == path:
                return sp

    def getTagByName(self, name):
        tags = self.getTags(True)
        for id, tag in tags.items():
            if tag.get('name') == name:
                return tag

    def getDoctypeByName(self, name):
        dts = self.getDoctypes(True)
        for id, dt in dts.items():
            if dt.get('name') == name:
                return dt

    def createStoragePaths(self, cabinets):
        sps = {}

        for cid, c in cabinets.items():
            full_path = c.get("full_path").replace(" / ", "/")
            path = full_path.split("/")
            new_path = []
            for p in path:
                if re.match('^[0-9]{4}$', p):
                   p = "{created_year}"
                new_path.append(p)
            path = "/".join(new_path)
            existing = self.getStoragePathByPath(path)
            if not existing:
                existing = self.getStoragePathByPath(path + "/{created_year}")
            if existing:
                self.cabinets_sp[cid] = existing.get('id')
            else:
                data = {
                    "name": path.replace("/", " ").replace(" {created_year}", " by year"),
                    "path": path + "/{title}",
                    "matching_algorithm": 6
                }
                response = postapi(self.url + "/api/storage_paths/", data, self.getHeaders())
                self.cabinets_sp[cid] = response.json().get('id')
        return self.getStoragePaths(True)

    def createTags(self, tags):
        for tid, t in tags.items():
            existing = self.getTagByName(t.get("name"))
            if existing:
                self.tags_tag[tid] = existing.get('id')
            else:
                data = {
                    "name": t.get("name"),
                    "color": t.get("colour"),
                    "matching_algorithm": 6
                }
                response = postapi(self.url + "/api/tags/", data, self.getHeaders())
                self.tags_tag[tid] = response.json().get('id')
        return self.getTags(True)

    def createDoctypes(self, doctypes):
        newdts = {}

        for dtid, dt in doctypes.items():
            existing = self.getDoctypeByName(dt.get("name"))
            if existing:
                self.doctype_doctype[dtid] = existing.get('id')
            else:
                data = {
                    "name": dt.get("name"),
                    "matching_algorithm": 6
                }
                response = postapi(self.url + "/api/document_types/", data, self.getHeaders())
                self.doctype_doctype[dtid] = response.json().get('id')
        return self.getDoctypes(True)

    def createDoc(self, file, doc):
        doctype = self.doctype_doctype[doc.get('document_type')]
        tags = []
        for t in doc.get('tags'):
            tags.append(self.tags_tag[t])
        data = {
            "title":            doc.get('name').replace(".pdf", ""),
            "tags":             tags,
            "document_type":    doctype,
        }

        files = {
            "document":         open(file, "rb")
        }
        response = postapi(self.url + "/api/documents/post_document/", data=data, files=files, headers=self.getHeaders())

    def setStoragePath(self, doc):
        doctype = self.doctype_doctype[doc.get('document_type')]
        if len(doc.get('cabinets')) > 0:
            storage_path = self.cabinets_sp[doc.get('cabinets')[0]]
        else:
            return

        d = self.getDocByName(doc.get('name'))
        if d:
            id = d.get("id")
            data = {
                "storage_path":     storage_path
            }

            response = patchapi(self.url + "/api/documents/" + str(id) + "/", data=data, headers=self.getHeaders())

    def getHeaders(self):
        return {
            'Authorization': f'Token {self.token}',
            'Accept': 'application/json; version=2'
        }

if __name__ == "__main__":
    main()

